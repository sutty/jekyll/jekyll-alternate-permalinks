# Changelog

## 2020-10-26 -- v0.1.2

Use `Jekyll::PathManager`

## 2020-10-26 -- v0.1.1

Minor refactorization and allows to use [frontmatter
defaults](https://jekyllrb.com/docs/configuration/front-matter-defaults/).

## 2019-10-31 -- v0.1.0

Initial release.

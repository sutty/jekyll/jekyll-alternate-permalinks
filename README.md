# Alternate permalinks

Prevents broken links when renaming articles by keeping a list of
previous permalinks and creating links between them.

## Installation

Add this line to your application's Gemfile:

```ruby
group :jekyll_plugins do
  gem 'jekyll-alternate-permalinks'
end
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-alternate-permalinks

## Usage

### Configuration

Add the plugin to the plugins array in `_config.yml`:

```yaml
plugins:
- jekyll-alternate-permalinks
```

### In pages and posts

Add the `alternate-permalinks` array to any file that supports a front
matter (pages, collections, posts).

```yaml
---
title: An article
permalink: an-article/
alternate-permalinks:
- an-article.html
- ?an-article
- articles/an-article/
---

Content
```

When the site is generated, all the alternate permalinks will be
symlinked to the current permalink (specified or auto-generated).

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and tags, and push the
`.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-alternate-permalinks>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-alternate-permalinks project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).


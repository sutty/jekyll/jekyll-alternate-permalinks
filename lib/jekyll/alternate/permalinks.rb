# frozen_string_literal: true

require 'pathname'

module Jekyll
  module AlternatePermalinks
    extend self

    def render(page)
      return unless page.data['alternate-permalinks'].is_a? Array

      site = page.site

      # Get a pathname for the file
      path = Pathname.new(page.destination(site.dest))

      # Create a symlink for each alternate permalink
      page.data['alternate-permalinks'].each do |alt|
        alt = Jekyll::PathManager.join(alt, 'index.html') if alt.end_with? '/'

        alt_path = Pathname.new(site.in_dest_dir(alt))

        if alt_path.exist?
          Jekyll.logger.warn 'Permalinks:', "#{alt_path} already exists, skipping"
        else
          relative_path = path.relative_path_from(alt_path.dirname)

          Jekyll.logger.info 'Permalinks:', "Symlink from #{relative_path} to #{alt}"

          FileUtils.mkdir_p(alt_path.dirname)
          File.symlink(relative_path, alt_path)
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'jekyll/alternate/permalinks'

Jekyll::Hooks.register :documents, :post_write do |document|
  Jekyll::AlternatePermalinks.render document
end

Jekyll::Hooks.register :pages, :post_write do |page|
  Jekyll::AlternatePermalinks.render page
end
